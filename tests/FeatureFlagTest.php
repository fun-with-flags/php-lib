<?php

namespace Test\FunWithFlags;

use FunWithFlags\FeatureFlag;
use Ramsey\Uuid\Uuid;
use PHPUnit\Framework\TestCase;
use DateTimeImmutable;

class FeatureFlagTest extends TestCase
{
    public function test_the_flag_has_a_uuid_set()
    {
        $now = new DateTimeImmutable();
        $sut = new FeatureFlag($now, $now);
        $this->assertTrue(Uuid::isValid($sut->getId()));
    }

    public function test_flag_is_enabled_when_date_is_between_start_and_end_date()
    {
        $start = new DateTimeImmutable('-7 days');
        $end = new DateTimeImmutable('+7 days');

        $sut = new FeatureFlag($start, $end);
        $result = $sut->isEnabled();

        $this->assertTrue($result);
    }
}
