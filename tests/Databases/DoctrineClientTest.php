<?php

namespace Tests\FunWithFlags\Databases;

use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DriverManager;
use FunWithFlags\Databases\DoctrineClient;

class DoctrineClientTest extends TestCase
{
    private $connection;

    /**
     * @before
     */
    public function configure_doctrine_database()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array(
            'dbname' => getenv('POSTGRES_DB'),
            'user' => 'postgres',
            'password' => getenv('POSTGRES_PASSWORD'),
            'host' => 'database',
            'driver' => 'pdo_pgsql',
        );

        $this->database = DriverManager::getConnection($connectionParams, $config);
        $this->create_test_data();
    }

    public function create_test_data()
    {
        $this->startTime = date('Y-m-d H:i:s', strtotime('-7 days'));
        $this->endTime = date('Y-m-d H:i:s', strtotime('+7 days'));
        $sql = 'INSERT INTO flags (id, start_time, end_time) VALUES (1, ?, ?)';
        $stmt = $this->database->prepare($sql);
        $stmt->bindValue(1, $this->startTime);
        $stmt->bindValue(2, $this->endTime);
        $stmt->execute();
    }

    /**
     * @after
     */
    public function remove_test_data()
    {
        $sql = 'TRUNCATE TABLE flags';
        $stmt = $this->database->prepare($sql);
        $stmt->execute();
    }

    public function test_a_single_key_can_be_retrieved()
    {
        $sut = new DoctrineClient($this->database);
        $result = $sut->getFlag(1);

        $this->assertInternalType('array', $result);
        $this->assertEquals(1, $result['id']);
        $this->assertEquals($result['start_time'], $this->startTime);
    }
}
