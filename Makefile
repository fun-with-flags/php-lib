.PHONY: test deps

test:
	@docker-compose up test

deps:
	@rm -rf vendor composer.lock
	@docker run --rm -it -u $(shell id -u) -v $(PWD):$(PWD) -w $(PWD) composer install
