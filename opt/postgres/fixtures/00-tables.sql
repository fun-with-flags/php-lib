CREATE TABLE flags (
 id serial PRIMARY KEY,
 start_time TIMESTAMP,
 end_time TIMESTAMP,
 enabled BOOLEAN NOT NULL
);

ALTER TABLE flags ALTER COLUMN enabled SET DEFAULT false;