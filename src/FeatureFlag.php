<?php

namespace FunWithFlags;

use Ramsey\Uuid\Uuid;
use DateTimeImmutable;

class FeatureFlag
{
    private $id;

    private $startTime;

    private $endTime;

    public function __construct(DateTimeImmutable $startTime, DateTimeImmutable $endTime)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStartTime(): DateTimeImmutable
    {
        return $this->startTime;
    }

    public function getEndTime(): DateTimeImmutable
    {
        return $this->endTime;
    }

    public function isEnabled(): bool
    {
        $now = new DateTimeImmutable('now');
        return $now >= $this->startTime && $now <= $this->endTime;
    }
}