<?php

namespace FunWithFlags\Databases;

use Doctrine\DBAL\Connection;
use FunWithFlags\FeatureFlagClient;

class DoctrineClient implements FeatureFlagClient
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllFlags(): array
    {

    }

    public function getFlag(string $key): array
    {
        $sql = 'SELECT * FROM flags WHERE id=?';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(1, $key);
        $stmt->execute();

        return $stmt->fetch();
    }
}
