<?php

namespace FunWithFlags;

interface FeatureFlagClient
{
    /**
     * Returns an array of flags which will be in array format
     */
    public function getAllFlags(): array;
    /**
     * Returns an array of data for a single feature flag
     */
    public function getFlag(string $key): array;
}
